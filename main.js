var fs = require('fs');
var pdfreader = require('pdfreader');
var exec = require('child_process').exec;

var ch = [
  '1dac114f65e9c8e3db71f925327f6e9b',
  '6bfd384ce0baffecc929743c8811f739',
  '7ffc61ad1896eeaded88387fe4f4dd5f',
  'e8923a8fa3d6f97cee284f0402fc9bb4'
]

function checkIfCheater(vpisna, tip){
  var command = "echo -n " + vpisna + " | md5sum"
  exec(command, function(error, stdout, stderr){
     stdout = stdout.replace('-',"");
     stdout = stdout.trim();
     for(var i = 0; i<ch.length;i++){
       ch[i] = ch[i].trim();
       if(stdout == ch[i]){
         var string = stdout + "  " + vpisna + " " + tip + "\n";
         console.log(string);
       }
     }
   });
}

//ISRM
  fs.readFile("isrm.pdf", (err, pdfBuffer) => {
    new pdfreader.PdfReader().parseBuffer(pdfBuffer, function(err, item){
      if (err){
        callback();
      }
      else if (!item){
        callback();
      }
      else if (item.text){
        if(item.text.length>3 && /^\d+$/.test(item.text)){
          checkIfCheater(item.text, 'isrm');
        }
      }
    });
  });
////////////////////////////////////////////////////////////////////////////////////////////

//FRI
  fs.readFile("fri.pdf", (err, pdfBuffer) => {
    new pdfreader.PdfReader().parseBuffer(pdfBuffer, function(err, item){
      if (err){
        callback();
      }
      else if (!item){
        callback();
      }
      else if (item.text){
        if(item.text.length>3 && /^\d+$/.test(item.text)){
          checkIfCheater(item.text, 'fri');
        }
      }
    });
  });
////////////////////////////////////////////////////////////////////////////////////////////

//MULTIMEDIJCI (+ frijevci....)
  fs.readFile("mult.pdf", (err, pdfBuffer) => {
    new pdfreader.PdfReader().parseBuffer(pdfBuffer, function(err, item){
      if (err){
        callback();
      }
      else if (!item){
        callback();
      }
      else if ( item.text != undefined && item.text.indexOf("6417") != -1){
          checkIfCheater(item.text, 'multimedija');
      }
    });
  });
////////////////////////////////////////////////////////////////////////////////////////////
